A CLI-based strategy game

## Author
* Senapati Sang Diwangkara
* [Gabriel Bentara Raphael](https://github.com/yukinotenshi)
* [M. Ridho Pratama](https://github.com/ridho9)
* [Jonathan Alvaro](https://github.com/oathofoblivion)
* [M. Abdullah Munir](https://github.com/mabdullahmunir)